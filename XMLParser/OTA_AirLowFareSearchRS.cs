﻿
/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class OTA_AirLowFareSearchRS
{

    private OTA_AirLowFareSearchRSPricedItineraries pricedItinerariesField;

    /// <remarks/>
    public OTA_AirLowFareSearchRSPricedItineraries PricedItineraries
    {
        get
        {
            return this.pricedItinerariesField;
        }
        set
        {
            this.pricedItinerariesField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class OTA_AirLowFareSearchRSPricedItineraries
{

    private OTA_AirLowFareSearchRSPricedItinerariesPricedItinerary pricedItineraryField;

    /// <remarks/>
    public OTA_AirLowFareSearchRSPricedItinerariesPricedItinerary PricedItinerary
    {
        get
        {
            return this.pricedItineraryField;
        }
        set
        {
            this.pricedItineraryField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class OTA_AirLowFareSearchRSPricedItinerariesPricedItinerary
{

    private OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItinerary airItineraryField;

    private OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryPricingInfo airItineraryPricingInfoField;

    /// <remarks/>
    public OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItinerary AirItinerary
    {
        get
        {
            return this.airItineraryField;
        }
        set
        {
            this.airItineraryField = value;
        }
    }

    /// <remarks/>
    public OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryPricingInfo AirItineraryPricingInfo
    {
        get
        {
            return this.airItineraryPricingInfoField;
        }
        set
        {
            this.airItineraryPricingInfoField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItinerary
{

    private OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryOriginDestinationOptions originDestinationOptionsField;

    /// <remarks/>
    public OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryOriginDestinationOptions OriginDestinationOptions
    {
        get
        {
            return this.originDestinationOptionsField;
        }
        set
        {
            this.originDestinationOptionsField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryOriginDestinationOptions
{

    private OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryOriginDestinationOptionsOriginDestinationOption originDestinationOptionField;

    /// <remarks/>
    public OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryOriginDestinationOptionsOriginDestinationOption OriginDestinationOption
    {
        get
        {
            return this.originDestinationOptionField;
        }
        set
        {
            this.originDestinationOptionField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryOriginDestinationOptionsOriginDestinationOption
{

    private OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryOriginDestinationOptionsOriginDestinationOptionFlightSegment flightSegmentField;

    /// <remarks/>
    public OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryOriginDestinationOptionsOriginDestinationOptionFlightSegment FlightSegment
    {
        get
        {
            return this.flightSegmentField;
        }
        set
        {
            this.flightSegmentField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryOriginDestinationOptionsOriginDestinationOptionFlightSegment
{

    private OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryOriginDestinationOptionsOriginDestinationOptionFlightSegmentDepartureAirport departureAirportField;

    private OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryOriginDestinationOptionsOriginDestinationOptionFlightSegmentArrivalAirport arrivalAirportField;

    private string marketingAirlineField;

    private System.DateTime departureDateTimeField;

    private System.DateTime arrivalDateTimeField;

    private string flightNumberField;

    private byte journeyIndicatorField;

    private string cabinClassField;

    /// <remarks/>
    public OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryOriginDestinationOptionsOriginDestinationOptionFlightSegmentDepartureAirport DepartureAirport
    {
        get
        {
            return this.departureAirportField;
        }
        set
        {
            this.departureAirportField = value;
        }
    }

    /// <remarks/>
    public OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryOriginDestinationOptionsOriginDestinationOptionFlightSegmentArrivalAirport ArrivalAirport
    {
        get
        {
            return this.arrivalAirportField;
        }
        set
        {
            this.arrivalAirportField = value;
        }
    }

    /// <remarks/>
    public string MarketingAirline
    {
        get
        {
            return this.marketingAirlineField;
        }
        set
        {
            this.marketingAirlineField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public System.DateTime DepartureDateTime
    {
        get
        {
            return this.departureDateTimeField;
        }
        set
        {
            this.departureDateTimeField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public System.DateTime ArrivalDateTime
    {
        get
        {
            return this.arrivalDateTimeField;
        }
        set
        {
            this.arrivalDateTimeField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string FlightNumber
    {
        get
        {
            return this.flightNumberField;
        }
        set
        {
            this.flightNumberField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public byte JourneyIndicator
    {
        get
        {
            return this.journeyIndicatorField;
        }
        set
        {
            this.journeyIndicatorField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string CabinClass
    {
        get
        {
            return this.cabinClassField;
        }
        set
        {
            this.cabinClassField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryOriginDestinationOptionsOriginDestinationOptionFlightSegmentDepartureAirport
{

    private string locationCodeField;

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string LocationCode
    {
        get
        {
            return this.locationCodeField;
        }
        set
        {
            this.locationCodeField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryOriginDestinationOptionsOriginDestinationOptionFlightSegmentArrivalAirport
{

    private string locationCodeField;

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string LocationCode
    {
        get
        {
            return this.locationCodeField;
        }
        set
        {
            this.locationCodeField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryPricingInfo
{

    private OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryPricingInfoItinTotalFare itinTotalFareField;

    private string quoteIDField;

    /// <remarks/>
    public OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryPricingInfoItinTotalFare ItinTotalFare
    {
        get
        {
            return this.itinTotalFareField;
        }
        set
        {
            this.itinTotalFareField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string QuoteID
    {
        get
        {
            return this.quoteIDField;
        }
        set
        {
            this.quoteIDField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryPricingInfoItinTotalFare
{

    private OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryPricingInfoItinTotalFareTotalFare totalFareField;

    /// <remarks/>
    public OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryPricingInfoItinTotalFareTotalFare TotalFare
    {
        get
        {
            return this.totalFareField;
        }
        set
        {
            this.totalFareField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class OTA_AirLowFareSearchRSPricedItinerariesPricedItineraryAirItineraryPricingInfoItinTotalFareTotalFare
{

    private ushort amountField;

    private string currencyCodeField;

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public ushort Amount
    {
        get
        {
            return this.amountField;
        }
        set
        {
            this.amountField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string CurrencyCode
    {
        get
        {
            return this.currencyCodeField;
        }
        set
        {
            this.currencyCodeField = value;
        }
    }
}

